# Teste de UX/UI

Leia primeiro toda a proposta, faça sua estimativa de horas do planejamento a ser realizado, explique a estratégia a ser seguida e envie um e-mail com o título **[UX/UI] Teste - Estimativa** para beawsome@waac.com.br.

## Desafio Waac Social

Este teste tem como objetivo avaliar as competências de raciocínio estratégico, criatividade
para solucionar problemas e os conhecimentos em design de interface e experiência do
usuário.

## Requisitos

A rede de hotéis Garden Hotéis vinha tendo seguidas quedas de ocupação em sua rede.
O aumento da procura por hospedagem compartilhada (Hostels) e o bed & breakfast (Arbnb)
fez com que a empresa mudasse de estratégia e resolvesse oferecer novos atrativos para
fidelizar os frequentadores de seus hotéis. Para isso, a empresa criou o seu programa de
fidelidade que proporciona vantagens exclusivas aos seus clientes. Em parceria com diversas
empresas do ramo varejista o programa oferece recompensas como descontos em sua
própria rede de hotéis. O programa consiste em acumular pontos para trocar por benefícios.

## Acúmulo de Pontos:

1. Cada vez que o cliente se hospeda em um dos hotéis Garden, ele pontua. A pontuação
varia de acordo com o quarto escolhido. Quanto mais vezes ele se hospeda nos hotéis
da rede mais ponto ele acumula;
2. O cliente pode pontuar também comprando produtos de empresas que sejam
parceiras do Garden. Cada produto possui um valor em pontos e cada vez que o cliente
compra em um estabelecimento parceiro ele acumula pontos no seu cartão de
fidelidade;

## Trocando os Pontos:

Com os pontos acumulados o cliente pode trocar por descontos em futuras
hospedagens;
A ideia de vantagens em futuras hospedagens atraiu novos clientes e boa parte aderiu ao
programa de fidelidade. No entanto, a necessidade de utilizar um cartão de crédito exclusivo
com a bandeira do hotel não agradou e acabou gerando um incômodo aos hóspedes que
gostariam de pontuar utilizando qualquer forma de pagamento. Outro ponto negativo é em
relação aos parceiros que não tiveram um aumento significativo de clientes. Eles alegam
que os frequentadores dos hotéis não sabem exatamente com quais produtos elas podem
pontuar, pois as informações não estão claras no site do programa de fidelidade.
Analisando o cenário acima, crie uma estratégia para que o programa de fidelidade atenda
melhor seus clientes e parceiros, apontando soluções para os problemas apresentados.
Para ajudar a estratégia, apresente também um nome e uma identidade visual para o
programa.

## Finalizando o teste

As dúvidas para a resolução da lógica do problema faz parte do teste, portanto ao passar os 7 dias mande um e-mail com o título **[UX/UI] Teste - Finalização** para beawsome@waac.com.br com as artes desenvolvidas.